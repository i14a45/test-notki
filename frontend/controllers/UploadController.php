<?php


namespace frontend\controllers;


use common\components\VideoUploadHandler;
use yii\web\Controller;

/**
 * Class UploadController
 * @package frontend\controllers
 */
class UploadController extends Controller
{
    public function actionIndex()
    {
        $handler = new VideoUploadHandler();

        $handler->getResponse()->send();
    }
}