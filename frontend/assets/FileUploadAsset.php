<?php


namespace frontend\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FileUploadAsset extends AssetBundle
{
    public $sourcePath = '@npm/blueimp-file-upload';

    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload.js'
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}