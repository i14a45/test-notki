<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use frontend\assets\FileUploadAsset;
use yii\helpers\Url;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;

frontend\assets\FileUploadAsset::register($this);
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

<!-- upload -->
                <?= Html::fileInput('uploadVideo', null, [
                    'id' => 'upload-video',
                ]) ?>
<div style="display:none">
                <?= Html::fileInput('file', null, [
                    'id' => 'fileupload',
                    'data-url' => Url::to(['/upload/index']),
                ]) ?>
</div>

                <?= $form->field($model, 'video_source')->hiddenInput()->label(false) ?>
<!-- /upload -->

<!-- прогресс загрузки -->
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
<!-- /прогресс загрузки -->

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<?php
// URL загрузчика

$url = Url::to(['/upload/index']);
$this->registerJs(
<<<JS
    function setUploadProgress(p){
        $('.progress-bar').css('width', p + '%').attr('aria-valuenow', p);
    }

    $('#fileupload').fileupload({
        minFileSize: 10 * 1024 * 1024,
        maxFileSize: 700 * 1024 * 1024,
        maxNumberOfFiles: 1,
        acceptFileTypes: 'video/*',
        maxChunkSize: 30000000,
        dataType: 'json',
        //maxRetries: 10,
        //retryTimeout: 500,
        add: function (e, data) {
            var that = this;
            console.log(data);
            $.getJSON('$url', {file: data.files[0].name}, function (result) {
                var file = result.file;
                data.uploadedBytes = file && file.size;
                $.blueimp.fileupload.prototype.options.add.call(that, e, data);
            });
        },
        /*send: function (e, data) {
            console.log('send');
            console.log(data);
        },*/
        /*chunkbeforesend: function (e, data) {
            console.log('chunkbeforesend');
            console.log(data);
        },
        chunksend: function (e, data) {
            console.log('chunksend');
            console.log(data);
        },*/
        chunkdone: function (e, data) {
            //console.log('chunkdone');
            var p = 100 * (data.total / data.uploadedBytes);
            setUploadProgress(p);
            //console.log(data);
        },
        /* 
        chunkfail: function (e, data) {
            console.log('chunkfail');
            console.log(data);
        },
        chunkalways: function (e, data) {
            console.log('chunkalways');
            console.log(data);
        },
        */
        done: function (e, data) {
            var file = data.result.file[0];
            $('#signupform-video_source').val(file.name);
            setUploadProgress(100);
            console.log(file);
        }
    });

    $('#upload-video').change(function(){
        var filesList = $(this).prop('files');
        if (filesList.length > 0) {
            $('#fileupload').fileupload('add', {files: filesList});
        }
    });
JS
);