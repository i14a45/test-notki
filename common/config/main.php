<?php

use yii\caching\FileCache;
use yii\rbac\DbManager;
use yii\web\DbSession;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
        'session' => [
            'class' => DbSession::class,
        ],
    ],
];
