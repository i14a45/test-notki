<?php


namespace common\components;


use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class VideoUploadHandler
 * @package frontend\components
 */
class VideoUploadHandler extends Component
{
    public $multiple = false;

    public $filenameParam = 'file';

    public $printResponse = true;

    //public $uploadTempDir = '@frontend/web/img/video/upload';
    public $uploadTempDir = '@www/img/video/upload';

    public $dirFromWebroot = '/img/video/upload';

    public $acceptMimeTypesRegexp = '/^video\/(.*)$/';

    public $minFileSize = 10 * 1024 * 1024;

    public $maxFileSize = 700 * 1024 * 1024;

    public $accessControlAllowOrigin = '*';

    public $accessControlAllowCreditentials = false;

    public $accessControlAllowMethods = [
        'OPTIONS',
        'HEAD',
        'GET',
        'POST',
        'PUT',
        'PATCH',
        'DELETE'
    ];

    /**
     * @var string[]
     */
    public $accessControlAllowHeaders = [
        'Content-Type',
        'Content-Range',
        'Content-Disposition'
    ];

    /**
     * @var string[]
     */
    public static $phpFileUploadErrors = [
        0 => 'Все заебись',
        1 => 'filesize > upload_max_filesize directive in php.ini',
        2 => 'filesize > MAX_FILE_SIZE directive in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk.',
        8 => 'A PHP extension stopped the file upload.',
    ];

    /**
     * @var string[]
     */
    public static $validationErrors = [
        'acceptMimeTypes' => 'wrong file type',
        'tooBig' => 'filesize too big',
        'tooSmall' => 'filesize too small'
    ];

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var Request
     */
    protected $request;


    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        $this->request = Yii::$app->request;

        switch ($this->request->method) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
            break;
            case 'GET':
                $this->get();
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->post();
                break;
            case 'DELETE':
                $this->delete();
                break;
            default:
        }
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param array $content
     */
    protected function generateResponse(array $content)
    {
        $response = $this->head();

        if ($this->request->headers->has('content-range')) {
            $files = $content[$this->filenameParam] ?? null;
            if (!empty($files) && is_array($files) && is_object($files[0]) && $files[0]->size) {
                $range = (int) $files[0]->size - 1;
                $response->headers->set('content-range', "Range: 0-$range");
            }
        }


        $response->content = json_encode($content);

        $this->response = $response;

    }

    /**
     * @return Response
     */
    protected function head()
    {
        $response = Yii::$app->response;

        $headers = [
            'Pragma' => 'no-cache',
            'Cache-Control' => 'no-store, no-cache, must-revalidate',
            'Content-Disposition' => 'inline; filename="files.json"',
            'X-Content-Type-Options' => 'nosniff',
        ];

        if ($this->accessControlAllowOrigin) {
            // send_access_control_headers
            $headers += [
                'Access-Control-Allow-Origin' => $this->accessControlAllowOrigin,
                'Access-Control-Allow-Credentials' => ($this->accessControlAllowCreditentials ? 'true' : 'false'),
                'Access-Control-Allow-Methods' => implode(', ', $this->accessControlAllowMethods),
                'Access-Control-Allow-Headers' => implode(', ', $this->accessControlAllowHeaders),
            ];
        }

        // send_content_type_header
        $headers['Vary'] = 'Accept';
        $accept = $this->request->headers->get('Accept');
        if (strpos($accept, 'application/json') !== false) {
            $headers['content-type'] = 'application/json';
        } else {
            $headers['content-type'] = 'text/plain';
        }

        foreach ($headers as $name => $value) {
            $response->headers->set($name, $value);
        }

        $this->response = $response;

        return $response;
    }

    protected function get()
    {
        $filename = $this->request->getQueryParam($this->filenameParam);
        $filename = $this->getFilename($filename);
        if ($filename) {
            $response = [
                $this->filenameParam => $this->getFileInfo($filename),
            ];
        } else {
            $response = [];
        }

        $this->generateResponse($response);
    }

    protected function post()
    {
        $uploadedFile = UploadedFile::getInstanceByName($this->filenameParam);

        $contentDispositionHeader =  $this->request->headers->get('content-disposition');
        $filename = $contentDispositionHeader ?
            rawurldecode(preg_replace('/(^[^"]+")|("$)/','', $contentDispositionHeader)) : null;

        $contentRangeHeader = $this->request->headers->get('content-range');
        $contentRange = $contentRangeHeader ? preg_split('/[^0-9]+/', $contentRangeHeader) : null;

        $sessionId = Yii::$app->session->id;

        $filename = $filename ?? $uploadedFile->baseName . '.' . $uploadedFile->extension;

        $file = new \stdClass();
        $file->name = $this->getFilename($filename);
        $file->size = (int) $contentRange[3] ?? $uploadedFile->size;
        $file->type = $uploadedFile->type;
        $file->error = null;

        $uploadDir = $this->getUploadPath();
        if ($this->validate($uploadedFile, $file, $uploadDir)) {
            $filePath = $this->getUploadPath($file->name);

            $appendToFile = $contentRange && is_file($filePath) && $file->size > filesize($filePath);
            if ($appendToFile) {
                $result = file_put_contents($filePath, fopen($uploadedFile->tempName,'r'), FILE_APPEND);
            } else {
                $result = $uploadedFile->saveAs($filePath);
            }

            if ($result === false) {
                $file->error = 'file save error';
            }

            clearstatcache(true, $filePath);
            $fileSize = filesize($filePath);
            $file->fileSize = $fileSize;
            if ($fileSize == $file->size) {
                // загружена последняя часть
                $file->url = $this->getDownloadUrl($file->name);
            } else {
                if (!$contentRange) {
                    unlink($filePath);
                    $file->error = 'abort';
                }
            }
        }

        $this->generateResponse([$this->filenameParam => [$file]]);
    }

    protected function delete()
    {

    }

    /**
     * @param UploadedFile $uploadedFile
     * @param object $file
     * @param string $uploadDir
     * @return bool
     */
    protected function validate($uploadedFile, &$file, $uploadDir)
    {
        if (!is_dir($uploadDir)) {
            try {
                $result = FileHelper::createDirectory($uploadDir);
            } catch (Exception $e) {
                $file->error = $e->getMessage();
                return false;
            }

            if (!$result) {
                $file->error = 'cant create upload directory';
                return false;
            }
        }

        if (!is_writable($uploadDir)) {
            $file->error = 'upload directory is not writable';
            return false;
        }

        if ($uploadedFile->getHasError()) {
            $file->error = static::$phpFileUploadErrors[$uploadedFile->error];
            return false;
        }

        if ($this->minFileSize && $file->size < $this->minFileSize) {
            $file->error = static::$validationErrors['tooSmall'];
            return false;
        }

        if ($this->maxFileSize && $file->size > $this->maxFileSize) {
            $file->error = static::$validationErrors['tooSmall'];
            return false;
        }

        if ($this->acceptMimeTypesRegexp && !preg_match($this->acceptMimeTypesRegexp, $file->type)) {
            $file->error = static::$validationErrors['acceptMimeTypes'];
            return false;
        }
        return true;
    }

    /**
     * @param $filename
     * @return string
     */
    protected function getFilename($filename)
    {
        $parts = pathinfo($filename);

        return md5($parts['filename'] . Yii::$app->session->id) . '.mp4';
    }

    /**
     * @param $filename
     * @return string
     */
    public function getDownloadUrl($filename): string
    {
        return $this->dirFromWebroot . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * @param $filename
     * @return string
     */
    protected function getUploadPath($filename = ''): string
    {
        return Yii::getAlias($this->uploadTempDir) . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * @param $filename
     * @return object|null
     */
    protected function getFileInfo($filename)
    {
        $filepath = $this->getUploadPath($filename);
        if (!is_file($filepath)) {
            return null;
        }

        clearstatcache(true, $filepath);
        $file = new \stdClass();
        $file->name = $filename;
        $file->size = filesize($filepath);
        $file->url = $this->getDownloadUrl($filename);

        return $file;
    }

    protected function basename($filepath, $suffix = null) {
        $splited = preg_split('/\//', rtrim ($filepath, '/ '));
        return substr(basename('X'.$splited[count($splited)-1], $suffix), 1);
    }
}