<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */
/* @var $model \yii\db\ActiveRecord */

$createTitle = $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass)));
$indexBreadcrumbsLabel = $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))));

$urlParams = $generator->generateUrlParams();
$modelClassName = Inflector::camel2words(StringHelper::basename($generator->modelClass));
$nameAttributeTemplate = '$model->' . $generator->getNameAttribute();
$titleTemplate = $generator->generateString('Update ' . $modelClassName . ': {name}', ['name' => '{nameAttribute}']);
if ($generator->enableI18N) {
    $updateTitle = strtr($titleTemplate, ['\'{nameAttribute}\'' => $nameAttributeTemplate]);
} else {
    $updateTitle = strtr($titleTemplate, ['{nameAttribute}\'' => '\' . ' . $nameAttributeTemplate]);
}

$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

$formId = Inflector::camel2id(StringHelper::basename($generator->modelClass)) . '-form';

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model->isNewRecord ? <?= $createTitle ?> : <?= $updateTitle ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $indexBreadcrumbsLabel ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= "<?php " ?>$form = ActiveForm::begin([
    'id' => '<?= $formId ?>',
]); ?>

<div class="card">
    <div class="card-body">

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "      <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>
    </div>
    <div class="card-footer">
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, ['class' => 'btn btn-sm btn-primary']) ?>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Cancel') ?>, ['index'], ['class' => 'btn btn-sm btn-default']) ?>
    </div>
</div>

<?= "<?php " ?>ActiveForm::end(); ?>
