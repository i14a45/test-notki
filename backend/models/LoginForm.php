<?php
namespace backend\models;

use common\models\User;
use Yii;

/**
 * Class LoginForm
 */
class LoginForm extends \common\models\LoginForm
{
    /**
     * @return bool
     */
    public function validateRole()
    {
        $user = $this->getUser();
        if (!$user || !array_key_exists(User::ROLE_ADMIN, Yii::$app->authManager->getRolesByUser($user->id))) {
            $this->addError('password', 'Incorrect username or password. (1)');
            return false;
        }

        return true;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && $this->validateRole()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }
}