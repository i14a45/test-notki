Yii 2 advanced application template with AdminLTE 3 theme
=

Installation
-
```
composer create-project --prefer-dist i14a45/yii2-adminlte-advanced-template app
```
or
```
git clone https://gitlab.com/i14a45/yii2-adminlte-advanced-template.git app
```
Install dependencies and initialize
```
cd app
composer install
./init
```
Modify database configuration in ```common/config/main-local.php```

Apply migrations
```
./yii migrate --migrationPath=@yii/rbac/migrations
./yii migrate
```
Create admin
```
./yii user/create-admin
```