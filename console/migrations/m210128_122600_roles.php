<?php

use yii\db\Migration;

/**
 * Class m210128_122600_roles
 */
class m210128_122600_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var \yii\rbac\DbManager $authManager */
        $authManager = Yii::$app->authManager;

        $backendUserRole = $authManager->createRole('backend-user');
        $authManager->add($backendUserRole);

        $adminRole = $authManager->createRole('admin');
        $authManager->add($adminRole);
        $authManager->addChild($adminRole, $backendUserRole);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $authManager = Yii::$app->authManager->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210128_122600_roles cannot be reverted.\n";

        return false;
    }
    */
}
