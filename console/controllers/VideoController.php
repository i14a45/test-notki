<?php


namespace console\controllers;


use common\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class VideoController extends Controller
{
    public function actionConvert()
    {
        /** @var User[] $users */
        $users = User::find()->where(['video_status' => User::VIDEO_STATUS_UPLOADED])->all();

        foreach ($users as $user) {
            //$input = Yii::getAlias('@frontend/web/') . $user->video_source;
            $i = Yii::getAlias('@www/img/video/source/') . $user->video_source;
            //$o = Yii::getAlias('@frontend/web') . $video;
            $o = Yii::getAlias('@www/img/video/converted/') . $user->video_source;

            $user->updateAttributes(['video_status' => User::VIDEO_STATUS_CONVERT]);
            exec("ffmpeg -i $i -c:v libx264 -c:a aac $o", $output, $code);
            if ($code = ExitCode::OK) {
                $user->updateAttributes(['video_status' => User::VIDEO_STATUS_CONVERTED]);
            } else {
                $user->updateAttributes(['video_status' => User::VIDEO_STATUS_CONVERT_ERROR]);
            }

        }
    }
}