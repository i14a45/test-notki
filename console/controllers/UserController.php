<?php

namespace console\controllers;

use common\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Users console controller
 */
class UserController extends Controller
{
    /**
     * Create action
     *
     * @return int
     */
    public function actionCreateAdmin()
    {
        $username = $this->prompt('Username', [
            'required' => true,
            'default' => 'admin',
        ]);

        $email = $this->prompt('E-mail', [
            'required' => true,
            'default' => 'admin@example.com',
        ]);

        $password = $this->prompt('Password', [
            'required' => true,
            'default' => 'admin',
        ]);

        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        if ($user->save()) {
            /** @var \yii\rbac\DbManager $authManager */
            $authManager = Yii::$app->authManager;
            $authManager->assign($authManager->getRole('admin'), $user->id);

            var_dump($user->attributes);
            return ExitCode::OK;
        }

        var_dump($user->errors);
        return ExitCode::UNSPECIFIED_ERROR;
    }
}